# web-scraping

## 当ツールについて
web上の画像ファイル、cssファイル、javascriptファイルを、ディレクトリ構成をそのままに取得するツール  

## 環境構築
1. git cloneする  
`git clone git@gitlab.com:tama06ss/web-scraping.git`

2. 「web-scraping」ディレクトリに移動  
`cd web-scraping`

3. 「node_modules」をインストール  
`npm i`  
yarnがインストール済みの場合は↓  
`yarn`

4. 「src」ディレクトリに移動  
`cd src`

5. 「node_modules」をインストール  
`npm i`  
yarnがインストール済みの場合は↓  
`yarn`

## コマンド
* デバッグ開始  
`npm run dev`  
yarnがインストール済みの場合は↓  
`yarn dev`

* ビルド  
`npm run dist`  
yarnがインストール済みの場合は↓  
`yarn dist`

## 使い方
1. テキストボックスにURLを入力

2. basic認証が掛かっている場合は、basic認証情報のラジオボックスを「有り」に変更し、idとpasswordを入力

3. 「取得開始」ボタンを押下し処理を実行する

4. デスクトップに「get-contents」というディレクトリが自動で生成され、そのディレクトリ内に各データが保存されている

## 動かない時
* 当ツールはインストール済みのGoogle Chromeをバックグラウンドで動かす処理になっているため、「src」ディレクトリ配下の「index.js」内の変数「executablePath」が、自身のPCのGoogle Chromeのパスと等しいか確認、異なっている場合は書き換えて使用する