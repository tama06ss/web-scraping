const puppeteer = require('./node_modules/puppeteer');
const axios = require('./node_modules/axios')
const fs = require('./node_modules/fs-extra');
const path = require('path');

// 読み込み対象のURL
let targetUrl;

// basic認証のauth情報
let auth;

let resultArea = document.getElementById('result-area');

const dirHome = process.env[process.platform == "win32" ? "USERPROFILE" : "HOME"];
const dirGetCnts = path.join(dirHome, 'Desktop/get-contents');

const executablePath = process.platform == "win32" ? "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe" : "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome";

async function exe() {
    const txtUrl = document.getElementById('txt-url').value;

    if (!txtUrl) {
        alert('データを取得したいURLを入力して下さい。');
        return;
    }

    if (!txtUrl.includes('http')) {
        alert('URLの形式が誤っています、再度入力してください。');
        return;
    }

    try {
        // 各情報を変数へ渡す
        targetUrl = new URL(txtUrl);
    } catch (err) {
        alert('URLの形式が誤っています、再度入力してください。');
        return;
    }

    auth = {
        username: document.getElementById('basic-id').value,
        password: document.getElementById('basic-password').value
    }

    const browser = await puppeteer.launch({
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox'
        ],
        executablePath: executablePath
    });

    try {
        // ローディング画面表示
        document.getElementById('loading').style.display = "flex";

        const page = await browser.newPage();

        // basic認証突破
        await page.authenticate({ username: auth.username, password: auth.password });

        // 表示したいURL
        await page.goto(targetUrl.href);

        // result-areaクリア
        resultArea.innerHTML = "";

        addLogResultArea('処理を開始しています...');

        // 保存先として、デスクトップに「get-contents」ディレクトリ作成
        creatingDirs(dirGetCnts);

        // html上の画像パスをすべて取得
        const imgPaths = await page.evaluate(() => {
            return Array.from(document.getElementsByTagName('img'))
                .map(img => img.src)
        })

        // 画像を保存
        await saveImgs(imgPaths);

        // htmlで読み込んでいる背景画像のパスを取得
        const backImgPaths = await page.evaluate(() => {
            let backImgPaths = [];
            let backImg;

            // img以外の全要素ループして、背景画像をすべてリスト化する
            for (dom of Array.prototype.slice.call(document.querySelectorAll(':not(img)'))) {
                backImg = window.getComputedStyle(dom).backgroundImage;

                if (backImg !== 'none' && backImg.includes('url')) {
                    backImgPaths.push(backImg.replace('url("', '').replace('")', ''));
                }
            }

            // 重複を削除して返す
            return [...new Set(backImgPaths)];
        })

        // 背景画像を保存
        await saveFilesFromPaths(backImgPaths, 'arraybuffer')

        // htmlで読み込んでいるcssのパスを取得
        const cssPaths = await page.evaluate(() => {
            return Array.from(document.getElementsByTagName('link'))
                .filter(link => link.type === 'text/css' || link.rel === 'stylesheet')
                .map(link => link.href);
        })

        // cssを保存
        await saveFilesFromPaths(cssPaths, 'document')

        // htmlで読み込んでいるjsのパスを取得
        const jsPaths = await page.evaluate(() => {
            return Array.from(document.getElementsByTagName('script'))
                .filter(script => script.src)
                .map(script => script.src);
        })

        // jsを保存
        await saveFilesFromPaths(jsPaths, 'document')

    } catch (err) {
        addLogResultArea('エラーが発生しました。');
        addLogResultArea(err);
    } finally {
        addLogResultArea('処理が完了しました。');
        alert('処理が完了しました。');

        setTimeout(() => {
            document.getElementById('loading').style.display = "none";
        }, 1000);

        browser.close();
    }
};

// ディレクトリ作成
function creatingDirs(dirPath) {
    try {
        // ディレクトリの存在チェック、ディレクトリがない場合例外投げる
        fs.statSync(dirPath)
    } catch (err) {
        // ディレクトリがない場合は作成
        fs.mkdirsSync(dirPath);
    }
}

// urlからファイル名を削除して返す
function filenameDeletingFromURL(url) {
    // urlの末尾に拡張子が入っている場合は、ファイル名を削除
    if (path.extname(url)) {
        url = url.replace(path.basename(url), '');
    }

    return url;
}

// 画像を保存
async function saveImgs(imgPaths) {

    // ファイルデータ
    let fileData;

    for (let imgPath of imgPaths) {
        // 画像パス確認
        if (!imgPath.includes('http')) {
            // srcの先頭が「/」=ルートパスと判定
            if (imgPath.charAt(0) === '/') {
                // ルートパス → 絶対パス
                imgPath = targetUrl.origin + imgPath;
            } else {
                // 相対パス → 絶対パス
                imgPath = path.join(targetUrl.href, imgPath);
            }
        }

        // 画像を取得
        fileData = await getFileRequest(imgPath, 'arraybuffer')

        // 取得した画像を保存
        await saveFile(fileData, imgPath);
    }
}

// パスの配列を元にファイルを保存
async function saveFilesFromPaths(filePaths, fileType) {
    for (let filePath of filePaths) {
        let fileData = await getFileRequest(filePath, fileType)

        if (!fileData) {
            continue;
        }

        await saveFile(fileData, filePath);
    }
}

// ファイルを保存
async function saveFile(fileData, filePath) {

    let fileSavePath = path.join(dirGetCnts, new URL(filePath).pathname);

    if (!path.extname(fileSavePath)) {
        addLogResultArea(`「${fileSavePath}」は拡張子が存在しないため、保存を中止しました。`);
        return;
    }

    // fileSavePathからディレクトリのパスを抽出し、ディレクトリ作成
    creatingDirs(filenameDeletingFromURL(fileSavePath));

    try {
        if (typeof fileData === 'string') {
            // テキストファイル保存
            fs.writeFile(fileSavePath, fileData);
        } else {
            // バイナリファイル保存
            fs.writeFile(fileSavePath, Buffer.from(fileData), 'binary');
        }
    } catch (err) {
        addLogResultArea(`${fileSavePath}の保存に失敗しました。`);
        addLogResultArea(err);
    }
}

// ファイル情報取得
async function getFileRequest(filePath, saveType) {
    try {
        // リクエスト
        const res = await axios.get(filePath, {
            responseType: saveType,
            auth: auth
        });

        return res.data;

    } catch (err) {
        addLogResultArea(`${filePath}のリクエストに失敗しました。`);
        addLogResultArea(err);

        return null;
    }
}

// result-areaにログを追加
function addLogResultArea(logText) {
    let log = document.createElement('p');

    log.innerText = logText;

    resultArea.appendChild(log);
}

// アラート
function alert(text) {
    let p = document.createElement('p');

    p.innerText = text;

    let alertArea = document.getElementById('alert-area');
    alertArea.appendChild(p);

    setTimeout(() => {
        p.classList.add('fade-out');
        setTimeout(() => {
            alertArea.innerHTML = "";
        }, 1000);
    }, 3000);
}